import React from 'react'
import Photobox from '../../components/PhotoBox/Photobox'
import img from "../../assets/images/avatar.png"
import Button from "../../components/Button/Button"
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div className='main'>
      <Photobox
        avatar={img}
        name="John Doe"
        title="Programmer. Creative. Innovator"
        description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
        Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque "
      />
      <Link to="/inner"><Button text='Know more'/></Link>
    </div>
  )
}

export default Home
import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { fetchEdData } from '../../redux/thunks/edDataFetch'
import Panel from "../../components/Panel/Panel"
import Box from '../../components/Box/Box'
import Expertise from '../../components/Expertise/Expertise'
import Feedback from "../../components/Feedback/Feedback"
import pic from "../../assets/images/user.png"
import Portfolio from '../../components/Portfolio/Portfolio'
import Timeline from '../../components/TimeLine/Timeline'
import Contact from '../../components/Address/Contact'
import Skills from '../../components/Skills/Skills'
import server from '../../services/server'


const DUMMY_DATA = [
  {
    date: '2013-2014',
    info: {
      company: 'Google',
      job: 'Front-end developer / php programmer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      id: 0
    }
  },
  {
    date: '2012',
    info: {
      company: 'Twitter',
      job: 'Web developer',
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      id: 1
    }
  }
]
const DUMMY_TEXT = `Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
Aenean commodo ligula eget dolor.Aenean massa.Cum sociis natoque 
penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. 
Nulla consequat massa quis enim.Donec pede justo, fringilla vel,
aliquet nec, vulputate eget, arcu.In enim justo, rhoncus ut, imperdiet a,
venenatis vitae, justo.Nullam dictum felis eu pede mollis pretium.Integer
tincidunt.Cras dapibus.Vivamus elementum semper nisi.Aenean vulputate eleifend
tellus.Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.Aliquam 
lorem ante, dapibus in, viverra quis, feugiat a, tellus.Phasellus viverra nulla ut metus 
varius laoreet.Quisque rutrum.Aenean imperdiet.Etiam ultricies nisi vel augue.Curabitur 
ullamcorper ultricies nisi.Nam eget dui.Etiam rhoncus.Maecenas tempus, tellus eget 
condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque`

const FEEDBACK_DATA = [
  {
    feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
    reporter: { photoUrl: pic, name: 'John Doe', citeUrl: 'https://www.citeexample.com', id: 0 }
  },
  {
    feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
    reporter: { photoUrl: pic, name: 'John Doe', citeUrl: 'https://www.citeexample.com', id: 1 }
  }]


const Inner = () => {
  const [active, setActive] = useState(true)
  const educationData = useSelector(state => state.education)
  const dispatch = useDispatch()

  const activeHandler = (state) => {
    setActive(state)
  }

  useEffect(() => {
    server()
    dispatch(fetchEdData("/api/educations"))
  }, [dispatch])

  return (
    <div className='inner'>
      <Panel func={activeHandler} />
      <div className={active ? 'content' : 'content wide'}>
        <Box
          title='About me'
          content={DUMMY_TEXT}
        />
        <Timeline data={educationData} />
        <Expertise data={DUMMY_DATA} />
        <Skills />
        <Portfolio />
        <Contact />
        <Feedback data={FEEDBACK_DATA} />
      </div>
    </div>
  )
}

export default Inner
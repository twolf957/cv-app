import { createSlice } from "@reduxjs/toolkit";

const skillSlice = createSlice({
    name: 'skills',
    initialState: {
        skillsIsOpen: false,
        skills: [],
        status: "done"
    },
    reducers: {
        changeActive: (state) => ({
            ...state,
            skillsIsOpen: !state.skillsIsOpen
        }),
        loadSkills: (state) => {
            if (localStorage.getItem("skills") !== null && localStorage.getItem("skills") !== 'null') {
                return {
                    ...state,
                    skills: JSON.parse(localStorage.getItem("skills"))
                }
            }
        },
        addSkill: (state, action)=>{
            localStorage.setItem("skills", JSON.stringify([...state.skills, action.payload]))
            return({
                ...state,
                skills: [...state.skills, action.payload]
            })
        }

    }
})

export const { addSkill, changeActive, loadSkills } = skillSlice.actions
export default skillSlice.reducer
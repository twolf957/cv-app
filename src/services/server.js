import { createServer } from "miragejs";

const TIMELINE_DATA = [
    {
        "id": 0,
        "date": 2001,
        "title": "Title 0",
        "text": "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n"
    },
    {
        "id": 1,
        "date": 2000,
        "title": "Title 1",
        "text": "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n"
    },
    {
        "id": 2,
        "date": 2012,
        "title": "Title 2",
        "text": "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n"
    }
    ,
    {
        "id": 3,
        "date": 2013,
        "title": "Title 3",
        "text": "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n"
    }
    ,
    {
        "id": 4,
        "date": 2017,
        "title": "Title 4",
        "text": "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n"
    }
    ,
    {
        "id": 5,
        "date": 2020,
        "title": "Title 5",
        "text": "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n"
    }
]

// eslint-disable-next-line
export default function () {
    let edCalled = false

    const server = createServer({
        routes() {
            this.get("/api/err", () => {
                throw new Error('Request rejected');
            }, { timing: 1500 })
            this.get("/api/educations", () => {
                edCalled = true;

                return {
                    data: TIMELINE_DATA
                }
            }, { timing: 3000 })
        },
    })
    const check = () => {
        if (edCalled) {
            console.log("server closing...");
            server.shutdown()
        } else {
            delay()
        }
    }
    const delay = async () => {
        await new Promise((res, rej) => setTimeout(()=>{check()}, 500))
    }
    check()
    return server
}
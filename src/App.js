import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home/Home';
import Inner from './pages/Inner/Inner';
import "./App.css"
function App() {
  return (
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/inner" element={<Inner/>}/>
    </Routes>
  );
}

export default App;

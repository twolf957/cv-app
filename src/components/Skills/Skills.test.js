import { getAllByRole, render } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import Skills from "./Skills";
import { Provider } from "react-redux";
import skillSlice from "../../redux/slices/skillSlice";
import educationSlice from "../../redux/slices/educationSlice";
import { configureStore } from "@reduxjs/toolkit";

const store = configureStore({
    reducer: {
        skills: skillSlice,
        education: educationSlice
    },
})
describe("Skills tests", () => {
    test("renders a Skills component", () => {
        const { getByText, getByRole } = render(
            <Provider store={store}>
                <Skills />
            </Provider>
        )
        const h1 = getByRole('heading')        

        expect(h1).toHaveClass("title")
        expect(h1).toBeInTheDocument()
    })
})
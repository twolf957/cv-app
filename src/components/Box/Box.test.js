import { render } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import Box from "./Box";

describe("Box tests", ()=> {
    test("renders a Box", ()=> {
        const {getByText} = render(<Box />)
        const h1 = getByText('text for title')
        const par = getByText('some text')

        expect(h1).toHaveClass("title")
        expect(h1).toHaveTextContent("text for title")
        expect(h1).toBeInTheDocument()
        expect(par).toHaveClass("box-content")
        expect(par).toHaveTextContent("some text")
        expect(par).toBeInTheDocument()
    })
    test("renders a Box with attributes", ()=> {
        const {getByText} = render(<Box title="some title" content="asd"/>)
        const h1 = getByText('some title')
        const par = getByText('asd')

        expect(h1).toHaveClass("title")
        expect(h1).toHaveTextContent("some title")
        expect(h1).toBeInTheDocument()
        expect(par).toHaveClass("box-content")
        expect(par).toHaveTextContent("asd")
        expect(par).toBeInTheDocument()
    })
})
import { getByRole, render } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import Feedback from "./Feedback";

describe("Feedback test", ()=> {
    test("renders a Feedback element", ()=> {
        const dummy_data = [{
            feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
            reporter: { photoUrl: '', name: 'John Doe', citeUrl: 'https://www.citeexample.com', id: 0 }
          }]
        const {getByText} = render(<Feedback data={dummy_data} />)
        const h1 = getByText('Feedback')
        const info = getByText(dummy_data[0].reporter.citeUrl)

        expect(h1).toHaveClass('title')
        expect(info).toBeInTheDocument()
    })
})
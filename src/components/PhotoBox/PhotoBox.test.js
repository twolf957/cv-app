import { render } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import Photobox from "./Photobox";

describe("photobox tests", ()=> {
    test("renders a photobox", ()=> {
        const {getByRole} = render(<Photobox />)
        const img = getByRole('img')

        expect(img).toHaveClass("avatar")
        expect(img).toBeInTheDocument()
    })
})
import { render } from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import { Provider } from "react-redux";
import skillSlice from "../../redux/slices/skillSlice";
import educationSlice from "../../redux/slices/educationSlice";
import { configureStore } from "@reduxjs/toolkit";
import Timeline from "./Timeline";

const store = configureStore({
    reducer: {
        skills: skillSlice,
        education: educationSlice
    },
})
describe("Timeline tests", () => {
    test("renders a Timeline component", () => {
        const { getByRole } = render(
            <Provider store={store}>
                <Timeline data={store.getState().education}/>
            </Provider>
        )
        const h1 = getByRole('heading')

        expect(h1).toHaveClass("title")
        expect(h1).toBeInTheDocument()
    })
})
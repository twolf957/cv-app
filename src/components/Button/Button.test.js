import { getByText, render } from "@testing-library/react"
import Button from "./Button"
import '@testing-library/jest-dom/extend-expect';

describe("Button tests", ()=> {
    test("renders a button", ()=> {
        const expectedText = "submit"
        const {getByText} = render(<Button type="submit"/>)
        const btn = getByText(expectedText)

        expect(btn).toHaveTextContent(expectedText)
        expect(btn).toBeInTheDocument()
    })
})
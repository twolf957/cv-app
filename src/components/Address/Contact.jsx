import React from 'react'
import { ReactComponent as Twitter } from "../../assets/images/twitter.svg"
import { ReactComponent as Phone } from "../../assets/images/phone-solid.svg"
import { ReactComponent as Email } from "../../assets/images/envelope-solid.svg"
import { ReactComponent as Fb } from "../../assets/images/facebook-f.svg"
import { ReactComponent as Skp } from "../../assets/images/skype.svg"
import ContactItem from './ContactItem'

const CONTACT_DATA = [
    {
        id: 1,
        title: "",
        info: "500 342 242",
        img: <Phone fill='rgb(38, 193, 126)' width='30px' />
    },
    {
        id: 2,
        title: "twolf957@gmail.com",
        info: "",
        img: <Email fill='rgb(38, 193, 126)' width='30px' />
    },
    {
        id: 3,
        title: "Twitter",
        info: "https://twitter.com/home",
        img: <Twitter fill='rgb(38, 193, 126)' width='30px' />
    },
    {
        id: 4,
        title: "Facebook",
        info: "https://fb.com/tedo",
        img: <Fb fill='rgb(38, 193, 126)' width='30px' />
    },
    {
        id: 5,
        title: "Skype",
        info: "donthaveskype.ge",
        img: <Skp fill='rgb(38, 193, 126)' width='30px' />
    },
]

const Contact = () => {
    return (
        <section className='contact' id='Contact'>
            <h1 className='title'>Contact</h1>
            <div>
                {CONTACT_DATA.map(e => <ContactItem title={e.title} info={e.info} img={e.img} key={e.id} />)}
            </div>
        </section>
    )
}

export default Contact
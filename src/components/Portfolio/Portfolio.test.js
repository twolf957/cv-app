import { render, waitFor } from "@testing-library/react"
import Portfolio from "./Portfolio"
import '@testing-library/jest-dom/extend-expect';
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";

describe("portfolio tests", () => {
    test("renders a portfolio", () => {

        const { getAllByRole } = render(<Portfolio />)
        const btn = getAllByRole('button')

        expect(btn[0]).toHaveTextContent("All")
        expect(btn[1]).toHaveTextContent("Code")
        expect(btn[2]).toHaveTextContent("UI")
        btn.forEach(e => expect(e).toBeInTheDocument())
    })
    test("button active", () => {
        const { getAllByTestId, getByText } = render(<Portfolio />)
        const h1 = getByText("Portfolio")
        const btns = getAllByTestId('my-button')

        expect(h1.tagName).toBe('H1')
        userEvent.click(btns[1])
        // render(<Portfolio />)
        expect(btns[1]).toHaveClass("active")
    })
})
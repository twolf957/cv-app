import React from "react"
import { getByText, getByTitle, render } from "@testing-library/react"
import Info from "./Info"
import '@testing-library/jest-dom/extend-expect';

describe("Info tests", ()=> {
    test("renders an Info", ()=> {
        const expectedText = "info test"
        const {getByText} = render(<Info text={expectedText} content={false}/>)
        const pElement = getByText(expectedText)

        expect(pElement).toHaveTextContent(expectedText)
        expect(pElement).toBeInTheDocument()
    })
})
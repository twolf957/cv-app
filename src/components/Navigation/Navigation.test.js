import { render } from "@testing-library/react"
import Navigation from "./Navigation"
import '@testing-library/jest-dom/extend-expect';

describe("Nav tests", () => {
    test('renders Navigation component with the correct initial state', () => {
        const { getAllByRole, getByText } = render(<Navigation />);

        const navigation = getAllByRole('navigation');
        navigation.forEach(e => expect(e).toBeInTheDocument())


        const aboutNavItem = getByText('About Me');
        expect(aboutNavItem).toHaveClass('text');
    });
})